#ifndef _OSAL_SCHEDULER_H_
#define _OSAL_SCHEDULER_H_
#include "osal_cfg.h"

void osal_scheduler_lock(void);
void osal_scheduler_unlock(void);

#endif /* _OSAL_SCHEDULER_H_ */
