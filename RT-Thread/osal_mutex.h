#ifndef _OSAL_MUTEX_H_
#define _OSAL_MUTEX_H_
#include "osal_cfg.h"
#include "rtthread.h"

typedef struct osal_mutex {
    rt_mutex_t mutex;
} osal_mutex_t;

int osal_mutex_init(osal_mutex_t *m, char *name);
int osal_mutex_lock(osal_mutex_t *m);
int osal_mutex_trylock(osal_mutex_t *m);
int osal_mutex_unlock(osal_mutex_t *m);
int osal_mutex_destroy(osal_mutex_t *m);

#endif /* _OSAL_MUTEX_H_ */
