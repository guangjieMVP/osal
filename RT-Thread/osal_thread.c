#include "osal_thread.h"
#include "osal_memory.h"

osal_thread_t *osal_thread_init( const char *name,
                                        void (*entry)(void *),
                                        void * const param,
                                        unsigned int stack_size,
                                        unsigned int priority,
                                        unsigned int tick)
{
    osal_thread_t *thread;

    thread = osal_memory_alloc(sizeof(osal_thread_t));

    if(RT_NULL == thread)
    {
        return RT_NULL;
    }

    /*modify thread creation method is dynamic creation, so thread exit rtos can recylcle the resource!*/
    thread->thread = rt_thread_create((const char *)name,
        entry, param,
        stack_size, priority, tick);
    
    if (thread->thread == RT_NULL)
    {
        return RT_NULL;    
    }
    else
    {
        return thread;    
    }

}

void osal_thread_startup(osal_thread_t* thread)
{
    rt_thread_startup(thread->thread);
}


void osal_thread_stop(osal_thread_t* thread)
{
    rt_thread_suspend(thread->thread);
    
}

void osal_thread_start(osal_thread_t* thread)
{
    rt_thread_resume(thread->thread);
}

void osal_thread_destroy(osal_thread_t* thread)
{
    rt_thread_delete(thread->thread);
    osal_memory_free(thread);
}


