#include "osal_tick.h"
#include "rtthread.h"

unsigned long osal_tick_get(void)
{
#if defined(OSAL_USE_OS)
    return (unsigned long)rt_tick_get();
#else
    return 0;
#endif
}

unsigned long osal_ms_to_tick(unsigned long ms)
{
#if defined(OSAL_USE_OS)
    return rt_tick_from_millisecond((rt_uint32_t)ms);
#else
    return 0;
#endif
}