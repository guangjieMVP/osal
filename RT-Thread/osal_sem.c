#include "osal_sem.h"

/* 
    RT_IPC_FLAG_FIFO, first in first out, not real time 
    RT_IPC_FLAG_PRIO, queue by thread priority
*/
int osal_sem_init(osal_sem_t *sem, char *name, unsigned int value)
{
#if defined (OSAL_USE_OS)
    sem->sem = rt_sem_create(name, value, RT_IPC_FLAG_PRIO);  
    if (RT_NULL == sem->sem) RT_ENOMEM;
#endif
    return 0;
}

int osal_sem_destroy(osal_sem_t *sem)
{
#if defined (OSAL_USE_OS)
    return rt_sem_delete(sem->sem);
#else
    return RT_EOK;
#endif
}

int osal_sem_wait(osal_sem_t *sem, unsigned int timeout)
{
#if defined (OSAL_USE_OS)
    return rt_sem_take(sem->sem, timeout);
#else
    return RT_EOK;
#endif
}

int osal_sem_trywait(osal_sem_t *sem)
{
#if defined (OSAL_USE_OS)
    return rt_sem_trytake(sem->sem);
#else
    return RT_EOK;
#endif
}

int osal_sem_post(osal_sem_t *sem)
{
#if defined (OSAL_USE_OS)
    return rt_sem_release(sem->sem);
#else
    return RT_EOK;
#endif
}
