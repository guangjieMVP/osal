#ifndef _OSAL_DELAY_H_
#define _OSAL_DELAY_H_
#include "osal_cfg.h"

int osal_delay_us(unsigned int us);
int osal_delay_ms(unsigned int ms);
int osal_delay_sec(unsigned int sec);

#endif /* _osal_DELAY_H_ */
