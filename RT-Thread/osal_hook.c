#include "osal_hook.h"
#include "rtthread.h"

void osal_idle_sethook(pf_hook_t hk)
{
#if defined (OSAL_USE_OS)
    rt_thread_idle_sethook(hk);
#endif
}

