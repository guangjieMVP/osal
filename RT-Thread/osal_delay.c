#include "osal_delay.h"
#include "rtthread.h"


int osal_delay_us(unsigned int us)
{
#if defined (OSAL_USE_OS)
    return 0;
#endif
}

/* 
 * rt_thread_delay 此函数基于RT-Thread 3.0+, 
 * 最新RT-Thread版本延时毫秒为 rt_thread_mdelay
 * RT-Thread 3.0+ 没有rt_thread_mdelay实现
 */
int osal_delay_ms(unsigned int ms)
{
#if defined (OSAL_USE_OS)
    return rt_thread_delay((rt_tick_t)ms);
#else
    return 0;
#endif
}

int osal_delay_sec(unsigned int sec)
{
#if defined (OSAL_USE_OS)
    int i;
    int ret;

    for (i = 0; i < sec; i++)
    {
        ret = osal_delay_ms(1000);
        if (ret != RT_EOK) return ret;
    }
    
    return RT_EOK;
#else
    return 0;
#endif 
}





