#ifndef _OSAL_TICK_H_
#define _OSAL_TICK_H_
#include "osal_cfg.h"

unsigned long osal_tick_get(void);
unsigned long osal_ms_to_tick(unsigned long ms);

#endif /* _OSAL_TICK_H_ */

