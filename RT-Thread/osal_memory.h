#ifndef _OSAL_MEMORY_H_
#define _OSAL_MEMORY_H_
#include "osal_cfg.h"
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>

#include <rtthread.h>

void *osal_memory_alloc(size_t size);
void *osal_memory_calloc(size_t num, size_t size);
void osal_memory_free(void *ptr);

#endif   /* _OSAL_MEMORY_H_ */
