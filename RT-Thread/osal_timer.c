#include "osal_timer.h"

static uint32_t osal_uptime_ms(void)
{
#if (RT_TICK_PER_SECOND == 1000)
    return (uint32_t)rt_tick_get();
#else
    rt_tick_t tick = 0u;

    tick = rt_tick_get() * 1000;
    return (uint32_t)((tick + RT_TICK_PER_SECOND - 1) / RT_TICK_PER_SECOND);
#endif
}

void osal_timer_init(osal_timer_t* timer)
{
    timer->time = 0;
}

void osal_timer_cutdown(osal_timer_t* timer, unsigned int timeout)
{
	timer->time = osal_uptime_ms();
    timer->time += timeout;
}

char osal_timer_is_expired(osal_timer_t* timer)
{
	return osal_uptime_ms() > timer->time ? 1 : 0;
}

int osal_timer_remain(osal_timer_t* timer)
{
    uint32_t now;

    now = osal_uptime_ms();
    if (timer->time <= now) {
        return 0;
    }

    return timer->time - now;
}

unsigned long osal_timer_now(void)
{
    return (unsigned long) osal_uptime_ms();
}

void osal_timer_usleep(unsigned long usec)
{
    uint32_t ms = 0;
    if(usec != 0) {
        ms = usec / 1000;
        if (ms == 0) {
            ms = 1;
        }
    }
    rt_thread_delay(ms);
}
