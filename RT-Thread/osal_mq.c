#include "osal_mq.h"

int osal_mq_init(osal_mq_t *mq, char *name, 
                     unsigned long msg_size, unsigned long max_msgs)
{
#if defined (OSAL_USE_OS)
    mq->mq = rt_mq_create(name, (rt_size_t)msg_size, (rt_size_t)max_msgs, RT_IPC_FLAG_FIFO);
    if (RT_NULL == mq->mq) return RT_ENOMEM;
#endif
    return 0;
}

int osal_mq_recv(osal_mq_t *mq, void *buf, unsigned long size)
{
#if defined (OSAL_USE_OS)
    return rt_mq_recv(mq->mq, buf, (rt_size_t)size, RT_WAITING_FOREVER);
#else
    return 0;
#endif
}

int osal_mq_send(osal_mq_t *mq, void *buf, unsigned long size)
{
#if defined (OSAL_USE_OS)
    return rt_mq_send(mq->mq, buf, (rt_size_t)size);
#else
    return 0;
#endif
}

int osal_mq_destroy(osal_mq_t *mq)
{
#if defined (OSAL_USE_OS)
    return rt_mq_delete(mq->mq);
#else
    return 0;
#endif
}
