#include "rtthread.h"
#include "osal_scheduler.h"

void osal_scheduler_lock(void)
{
#if defined OSAL_USE_OS
    rt_enter_critical();
#endif
}

void osal_scheduler_unlock(void)
{
#if defined OSAL_USE_OS
    rt_exit_critical();	
#endif
}
