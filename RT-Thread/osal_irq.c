#include "osal_irq.h"
#include "rtthread.h"
#include "rthw.h"

void osal_interrupt_enter(void)
{
#if defined (OSAL_USE_OS)
    rt_interrupt_enter();
#endif
}

void osal_interrupt_level(void)
{
#if defined (OSAL_USE_OS)
    rt_interrupt_leave();
#endif
}

void osal_interrupt_disable(void)
{
#if defined (OSAL_USE_OS)
    rt_hw_interrupt_disable();
#endif
}

void osal_interrupt_enable(long level)
{
#if defined (OSAL_USE_OS)
    rt_hw_interrupt_enable((rt_base_t)level);
#endif
}
