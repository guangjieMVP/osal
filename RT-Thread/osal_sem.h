#ifndef _OSAL_SEM_H_
#define _OSAL_SEM_H_
#include "osal_cfg.h"
#include "rtthread.h"

typedef struct osal_sem {
    rt_sem_t sem; 
}osal_sem_t;

int osal_sem_init(osal_sem_t *sem, char *name, unsigned int value);
int osal_sem_wait(osal_sem_t *sem, unsigned int timeout);
int osal_sem_trywait(osal_sem_t *sem);
int osal_sem_post(osal_sem_t *sem);

#endif /* _OSAL_SEM_H_ */
