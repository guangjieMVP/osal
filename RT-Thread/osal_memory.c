#include "osal_memory.h"
#include "string.h"

void *osal_memory_alloc(size_t size)
{
#if defined (OSAL_USE_OS)
    char *ptr;
    ptr = rt_malloc(size);
    memset(ptr, 0, size);
    return (void *)ptr;
#else
    return RT_NULL;
#endif
}

void *osal_memory_calloc(size_t num, size_t size)
{
#if defined (OSAL_USE_OS)
    return rt_calloc(num, size);
#else
    return RT_NULL;
#endif
}

void osal_memory_free(void *ptr)
{
#if defined (OSAL_USE_OS)
    rt_free(ptr);
#endif
}



