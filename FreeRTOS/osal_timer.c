#include "osal_timer.h"
#include "FreeRTOS.h"
#include "task.h"

static uint32_t osal_uptime_ms(void)
{
#if (configTICK_RATE_HZ == 1000)
    return (uint32_t)xTaskGetTickCount();
#else
    TickType_t tick = 0u;

    tick = xTaskGetTickCount() * 1000;
    return (uint32_t)((tick + configTICK_RATE_HZ - 1) / configTICK_RATE_HZ);
#endif
}

void osal_timer_init(osal_timer_t* timer)
{
    timer->time = 0;
}

void osal_timer_cutdown(osal_timer_t* timer, unsigned int timeout)
{
	timer->time = osal_uptime_ms();
    timer->time += timeout;
}

char osal_timer_is_expired(osal_timer_t* timer)
{
	return osal_uptime_ms() > timer->time ? 1 : 0;
}

int osal_timer_remain(osal_timer_t* timer)
{
    uint32_t now;

    now = osal_uptime_ms();
    if (timer->time <= now) {
        return 0;
    }

    return timer->time - now;
}

unsigned long osal_timer_now(void)
{
    return (unsigned long) osal_uptime_ms();
}

void osal_timer_usleep(unsigned long usec)
{

    TickType_t tick;

    if(usec != 0) {
        tick = usec / portTICK_PERIOD_MS;
        
        if (tick == 0)
            tick = 1;
    }

    vTaskDelay(tick);
}

