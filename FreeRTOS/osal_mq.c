#include "osal_mq.h"

int osal_mq_init(osal_mq_t *mq, char *name, 
                     unsigned long msg_size, unsigned long max_msgs)
{
    mq->mq = xQueueCreate((UBaseType_t )max_msgs, (UBaseType_t)msg_size);
    if (NULL == mq->mq) return -1;
    return 0;
}

int osal_mq_recv(osal_mq_t *mq, void *buf, unsigned long size)
{
    return xQueueReceive(mq->mq, buf, size, 0xFFFFFF);
}

int osal_mq_send(osal_mq_t *mq, void *buf, unsigned long size)
{
    return xQueueSendToBack(mq->mq, buf, 0);
}

int osal_mq_destroy(osal_mq_t *mq)
{
    return vQueueDelete(mq->mq);
}