#ifndef _OSAL_IRQ_H_
#define _OSAL_IRQ_H_

void osal_interrupt_enter(void);
void osal_interrupt_level(void);

void osal_interrupt_disable(void);
void osal_interrupt_enable(long level);

#endif /* _OSAL_IRQ_H_ */