#include "osal_hook.h"
#include "FreeRTOS.h"

static pf_hook_t  idle_hook = NULL;

void osal_idle_sethook(pf_hook_t hk)
{
    idle_hook = hk;
}

/* FreeRTOS 空闲线程钩子函数，不支持函数设置 */
void vApplicationIdleHook(void)
{
    if (NULL == idle_hook) return;

    idle_hook();
}

