#include "FreeRTOS.h"
#include "osal_scheduler.h"

void osal_scheduler_lock(void)
{
    taskENTER_CRITICAL();
}

void osal_scheduler_unlock(void)
{
    taskEXIT_CRITICAL();	
}