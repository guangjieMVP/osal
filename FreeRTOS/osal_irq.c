#include "osal_irq.h"
#include "FreeRTOS.h"

void osal_interrupt_enter(void)
{

}

void osal_interrupt_level(void)
{
    
}

void osal_interrupt_disable(void)
{
    portDISABLE_INTERRUPTS();
}

void osal_interrupt_enable(long level)
{
    level = (long)level;
    portENABLE_INTERRUPTS();
}