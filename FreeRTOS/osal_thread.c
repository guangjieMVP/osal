#include "osal_thread.h"
#include "osal_memory.h"

osal_thread_t *osal_thread_init( const char *name,
                                        void (*entry)(void *),
                                        void * const param,
                                        unsigned int stack_size,
                                        unsigned int priority,
                                        unsigned int tick)
{
    BaseType_t err;
    osal_thread_t *thread;
    
    thread = osal_memory_alloc(sizeof(osal_thread_t));

    (void)tick;

    err =  xTaskCreate(entry, name, stack_size, param, priority, &thread->thread);

    if(pdPASS != err) {
        osal_memory_free(thread);
        return NULL;
    }

    return thread;
}

void osal_thread_startup(osal_thread_t* thread)
{
    (void)thread;
}


void osal_thread_stop(osal_thread_t* thread)
{
    vTaskSuspend(thread->thread);
}

void osal_thread_start(osal_thread_t* thread)
{
    vTaskResume(thread->thread);
}

void osal_thread_destroy(osal_thread_t* thread)
{
    if (NULL != thread)
        vTaskDelete(thread->thread);
    
    osal_memory_free(thread);
}


