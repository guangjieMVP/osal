#include "osal_sem.h"

int osal_sem_init(osal_sem_t *sem, char *name, unsigned int value)
{
    sem->sem = xSemaphoreCreateCounting(name, 0xFFFFFFFF, value);  
    if (NULL == sem->sem) -1;
    return RT_EOK;
}

int osal_sem_destroy(osal_sem_t *sem)
{
    return vSemaphoreDelete(sem->sem);
}

int osal_sem_wait(osal_sem_t *sem, unsigned int timeout)
{
    return xSemaphoreTake(sem->sem, (TickType_t )timeout);
}

int osal_sem_trywait(osal_sem_t *sem)
{
    return xSemaphoreTake(sem->sem, 0);
}

int osal_sem_post(osal_sem_t *sem)
{
    return xSemaphoreGive(sem->sem);
}