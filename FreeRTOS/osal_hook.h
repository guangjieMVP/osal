#ifndef _OSAL_HOOK_H_
#define _OSAL_HOOK_H_

typedef void (*pf_hook_t)(void);

void osal_idle_sethook(pf_hook_t hk);

#endif /* _OSAL_HOOK_H_ */