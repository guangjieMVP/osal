#include "osal_memory.h"
#include "string.h"

#include "FreeRTOS.h"

void *osal_memory_alloc(size_t size)
{
    char *ptr;
    ptr = pvPortMalloc(size);
    memset(ptr, 0, size);
    return (void *)ptr;
}

void *osal_memory_calloc(size_t num, size_t size)
{
    return pvPortMalloc(num * size);
}

void osal_memory_free(void *ptr)
{
    vPortFree(ptr);
}



