#ifndef _OSAL_MEMORY_H_
#define _OSAL_MEMORY_H_
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>

void *osal_memory_alloc(size_t size);
void *osal_memory_calloc(size_t num, size_t size);
void osal_memory_free(void *ptr);

#endif
