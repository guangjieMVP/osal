#ifndef _OSAL_NET_SOCKET_H_
#define _OSAL_NET_SOCKET_H_

#include "network.h"
#include "mqtt_error.h"

#include "lwip/opt.h"
#include "lwip/sys.h"
#include "lwip/api.h"
#include <lwip/sockets.h>
#include "lwip/netdb.h"

#define OSAL_NET_PROTO_TCP  0 /**< The TCP transport protocol */
#define OSAL_NET_PROTO_UDP  1 /**< The UDP transport protocol */

int osal_net_socket_connect(const char *host, const char *port, int proto);
int osal_net_socket_recv(int fd, void *buf, size_t len, int flags);
int osal_net_socket_recv_timeout(int fd, unsigned char *buf, int len, int timeout);
int osal_net_socket_write(int fd, void *buf, size_t len);
int osal_net_socket_write_timeout(int fd, unsigned char *buf, int len, int timeout);
int osal_net_socket_close(int fd);
int osal_net_socket_set_block(int fd);
int osal_net_socket_set_nonblock(int fd);
int osal_net_socket_setsockopt(int fd, int level, int optname, const void *optval, socklen_t optlen);

#endif /* _OSAL_NET_SOCKET_H_ */
