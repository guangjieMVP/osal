#include "osal_tick.h"
#include "FreeRTOS.h"

unsigned long osal_tick_get(void)
{
    return (unsigned long)rt_tick_get();
}

unsigned long osal_ms_to_tick(unsigned long ms)
{
    TickType_t tick = pdMS_TO_TICKS(ms);
    return tick;
}