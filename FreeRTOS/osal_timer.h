#ifndef _OSAL_TIMER_H_
#define _OSAL_TIMER_H_

#include "FreeRTOS.h"

typedef struct osal_timer {
    uint32_t time;
} osal_timer_t;

void osal_timer_init(osal_timer_t* timer);
void osal_timer_cutdown(osal_timer_t* timer, unsigned int timeout);
char osal_timer_is_expired(osal_timer_t* timer);
int osal_timer_remain(osal_timer_t* timer);
unsigned long osal_timer_now(void);
void osal_timer_usleep(unsigned long usec);

#endif
