#include "osal_delay.h"
#include "FreeRTOS.h"

int osal_delay_us(unsigned int us)
{

}

int osal_delay_ms(unsigned int ms)
{
    TickType_t tick = pdMS_TO_TICKS(ms);
    return vTaskDelay(tick);
}

int osal_delay_sec(unsigned int sec)
{
    int i;
    int ret;

    for (i = 0; i < sec; i++)
    {
        ret = osal_delay_ms(1000);
        if (ret != RT_EOK) return ret;
    }
    
    return RT_EOK;
}





