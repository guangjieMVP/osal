#include "osal_mutex.h"

int osal_mutex_init(osal_mutex_t* m)
{
    m->mutex = xSemaphoreCreateMutex();
    return 0;
}

int osal_mutex_lock(osal_mutex_t* m)
{
    return xSemaphoreTake(m->mutex, portMAX_DELAY);
}

int osal_mutex_trylock(osal_mutex_t* m)
{
    return xSemaphoreTake(m->mutex, 0);
}

int osal_mutex_unlock(osal_mutex_t* m)
{
    return xSemaphoreGive(m->mutex);
}

int osal_mutex_destroy(osal_mutex_t* m)
{
    vSemaphoreDelete(m->mutex);
    return 0;
}
