#ifndef __OSAL_H__
#define __OSAL_H__
#include "osal_cfg.h"
#include "osal_delay.h"
#include "osal_thread.h"
#include "osal_hook.h"
#include "osal_irq.h"
#include "osal_sem.h"
#include "osal_timer.h"
#include "osal_tick.h"
#include "osal_mutex.h"
#include "osal_mq.h"

#define OSAL_WAITTING_FOREVER    (-1)

#endif /* __OSAL_H__ */
