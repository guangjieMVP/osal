#ifndef _OSAL_CFG_H_
#define _OSAL_CFG_H_

#define OSAL_VERSION      "1.2"

#if !defined (OSAL_VERSION)
	#error "Please define the OSAL version : OSAL_VERSION"
#endif

#define OSAL_USE_OS      

#endif /* _OSAL_CFG_H_ */
