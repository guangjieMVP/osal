#include "osal_mq.h"

int osal_mq_init(osal_mq_t *mq, char *name, 
                     unsigned long msg_size, unsigned long max_msgs)
{
#if defined (OSAL_USE_OS)
    mq->mq = lin_mq_create();
    if (NULL == mq->mq) return -1;
#endif
    return 0;
}

int osal_mq_recv(osal_mq_t *mq, void *buf, unsigned long size)
{
#if defined (OSAL_USE_OS)
    return lin_mq_get_msg(mq->mq, (char *)buf, (int)size);
#else
    return 0;
#endif
}

int osal_mq_send(osal_mq_t *mq, void *buf, unsigned long size)
{
#if defined (OSAL_USE_OS)
    return lin_mq_send_msg(mq->mq, (char *)buf, (int)size);
#else
    return 0;
#endif
}

int osal_mq_destroy(osal_mq_t *mq)
{
#if defined (OSAL_USE_OS)
    return lin_mq_destroy(mq->mq);
#else
    return 0;
#endif
}
