#include "osal_mutex.h"

int osal_mutex_init(osal_mutex_t* m)
{
    return pthread_mutex_init(&(m->mutex), NULL);
}

int osal_mutex_lock(osal_mutex_t* m)
{
    return pthread_mutex_lock(&(m->mutex));
}

int osal_mutex_trylock(osal_mutex_t* m)
{
    return pthread_mutex_trylock(&(m->mutex));
}

int osal_mutex_unlock(osal_mutex_t* m)
{
    return pthread_mutex_unlock(&(m->mutex));
}

int osal_mutex_destroy(osal_mutex_t* m)
{
    return pthread_mutex_destroy(&(m->mutex));
}
