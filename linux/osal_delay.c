#include "osal_delay.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <poll.h>
#include <stdint.h>
#include <sys/select.h>

static void sleep_ms(unsigned int ms)
{
    struct timeval delay;
	delay.tv_sec = 0;
	delay.tv_usec = ms * 1000; 
	select(0, NULL, NULL, NULL, &delay);
}

int osal_delay_us(unsigned int us)
{
    usleep(us);
    return 0;
}

int osal_delay_ms(unsigned int ms)
{
    sleep_ms(ms);
    return 0;
}

int osal_delay_sec(unsigned int sec)
{
    int i;
    int ret;

    for (i = 0; i < sec; i++)
    {
        ret = osal_delay_ms(1000);
        if (ret != 0) return ret;
    }
    
    return 0;
}





