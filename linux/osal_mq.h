#ifndef _OSAL_MQ_H_
#define _OSAL_MQ_H_
#include "osal_cfg.h"
#include "lin_msgqueue.h"

typedef struct osal_mq {
    lin_mq_t *mq;
}osal_mq_t;

int osal_mq_init(osal_mq_t *mq, char *name, 
                     unsigned long msg_size, unsigned long max_msgs);
int osal_mq_recv(osal_mq_t *mq, void *buf, unsigned long size);
int osal_mq_send(osal_mq_t *mq, void *buf, unsigned long size);
int osal_mq_destroy(osal_mq_t *mq);

#endif /* _OSAL_MQ_H_ */
