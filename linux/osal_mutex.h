#ifndef _OSAL_MUTEX_H_
#define _OSAL_MUTEX_H_
#include <pthread.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct osal_mutex {
    pthread_mutex_t mutex;
} osal_mutex_t;

int osal_mutex_init(osal_mutex_t* m);
int osal_mutex_lock(osal_mutex_t* m);
int osal_mutex_trylock(osal_mutex_t* m);
int osal_mutex_unlock(osal_mutex_t* m);
int osal_mutex_destroy(osal_mutex_t* m);

#ifdef __cplusplus
}
#endif

#endif
