#ifndef _OSAL_THREAD_H_
#define _OSAL_THREAD_H_

#include <pthread.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct osal_thread {
    pthread_t thread;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
} osal_thread_t;

osal_thread_t *osal_thread_init( const char *name,
                                        void (*entry)(void *),
                                        void * const param,
                                        unsigned int stack_size,
                                        unsigned int priority,
                                        unsigned int tick);
void osal_thread_startup(osal_thread_t* thread);
void osal_thread_stop(osal_thread_t* thread);
void osal_thread_start(osal_thread_t* thread);
void osal_thread_destroy(osal_thread_t* thread);

#ifdef __cplusplus
}
#endif

#endif
