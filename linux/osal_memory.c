#include "osal_memory.h"

void *osal_memory_alloc(size_t size)
{
    return malloc(size);
}

void *osal_memory_calloc(size_t num, size_t size)
{
    return calloc(num, size);
}

void osal_memory_free(void *ptr)
{
    free(ptr);
}



